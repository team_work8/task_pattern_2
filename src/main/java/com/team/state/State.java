package com.team.state;

import com.team.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {
    Logger LOGGER = LogManager.getLogger();

    default void goToDo(Controller controller) {
        LOGGER.info("You cannot go to toDo");
    }

    default void goToReview(Controller controller) {
        LOGGER.info("You cannot go to Review");
    }

    default void goToInProgress(Controller controller) {
        LOGGER.info("You cannot go to Progress");
    }

    default void goToDone(Controller controller) {
        LOGGER.info("You cannot go to Done");
    }

    default void addTask(Controller controller, String name, int userID, Integer deadline) {
        LOGGER.info("You cannot add task here");
    }

}
