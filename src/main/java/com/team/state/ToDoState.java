package com.team.state;

import com.team.controller.Controller;

public class ToDoState implements State {
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET = "\u001B[0m";

    public final void addTask(final Controller controller, final String name, int userID, Integer deadline) {
        controller.addTask(name, userID, deadline);
        System.out.print(ANSI_YELLOW);
        System.out.println("New task is added to list");
        System.out.print(ANSI_RESET);
    }

    public final void goToInProgress(final Controller controller) {
        controller.setState(new InProgressState());
        System.out.print(ANSI_GREEN);
        System.out.println("Task is in progress");
        System.out.print(ANSI_RESET);
    }

}
