package com.team.state;

import com.team.controller.Controller;
import com.team.task.Task;

public class ReviewState implements State {
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_RESET = "\u001B[0m";

    public final void goToInProgress(final Controller controller) {
        controller.setState(new InProgressState());
        System.out.print(ANSI_RED);
        System.out.println("Task is sent back to progress");
        System.out.print(ANSI_RESET);
    }

    public final void goToDone(final Controller controller) {
        controller.setState(new DoneState());
        for (Task task : controller.getTasks()) {
            task.changeDone();
        }
        System.out.print(ANSI_GREEN);
        System.out.println("Task is done");
        System.out.print(ANSI_RESET);
    }

}
