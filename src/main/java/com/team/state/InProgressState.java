package com.team.state;

import com.team.controller.Controller;

public class InProgressState implements State {
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_RESET = "\u001B[0m";

    public final void goToReview(final Controller controller) {
        controller.setState(new ReviewState());
        System.out.print(ANSI_GREEN);
        System.out.println("Task is sent to review");
        System.out.print(ANSI_RESET);

    }

    public final void goToDo(final Controller controller) {
        controller.setState(new ToDoState());
        System.out.println(ANSI_RED);
        System.out.println("Task is sent back to toDo");
        System.out.print(ANSI_RESET);
    }

}
