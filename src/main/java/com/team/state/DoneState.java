package com.team.state;

import com.team.controller.Controller;

import java.util.LinkedList;

public class DoneState implements State {
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";

    public final void goToDo(final Controller controller) {
        controller.setState(new ToDoState());
        System.out.print(ANSI_GREEN);
        System.out.println("Successfully went back to toDo");
        controller.setTasks(new LinkedList<>());
        System.out.print(ANSI_RESET);
    }
}
