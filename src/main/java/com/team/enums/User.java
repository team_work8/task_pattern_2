package com.team.enums;

public enum User {
    DAVID,
    JOHN,
    MATT,
    STEVE,
    KEVIN
}
