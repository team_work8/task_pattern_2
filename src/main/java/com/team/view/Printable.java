package com.team.view;

@FunctionalInterface
public interface Printable {
    void print();
}
