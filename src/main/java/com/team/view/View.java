package com.team.view;

import com.team.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private Scanner scanner = new Scanner(System.in);
    private Logger logger = LogManager.getLogger();
    private Map<String, String> options;
    private Map<String, Printable> methods;
    private Map<String, String> users;
    private Controller controller = new Controller();

    public View() {
        options = new LinkedHashMap<>();
        options.put("1", "1 - Add task to list");
        options.put("2", "2 - to do");
        options.put("3", "3 - Run all tasks");
        options.put("4", "4 - Review all tasks");
        options.put("5", "5 - Finish all tasks");
        options.put("6", "6 - Quit");

        users = new LinkedHashMap<>();
        users.put("1", "1 - David");
        users.put("2", "2 - John");
        users.put("3", "3 - Matt");
        users.put("4", "4 - Steve");
        users.put("5", "5 - Kevin");
        users.put("6", "6 - Quit");

        methods = new LinkedHashMap<>();
        methods.put("1", this::add);
        methods.put("2", this::toDo);
        methods.put("3", this::inProgress);
        methods.put("4", this::review);
        methods.put("5", this::done);
        methods.put("6", this::exit);
        interact();
    }

    private void showUsers() {
        logger.trace("~~~~~USERS~~~~~");
        for (String s : users.values()) {
            logger.trace(s);
        }
        logger.trace("~~~~~USERS~~~~~");
    }

    private void showMenu() {
        logger.trace("~~~~~MENU~~~~~");
        for (String s : options.values()) {
            logger.trace(s);
        }
        logger.trace("~~~~~MENU~~~~~");
    }

    private void exit() {
        logger.trace("Bye!");
        System.exit(0);
    }

    private void add() {
        logger.trace("Input the task: ");
        logger.trace("-->");
        scanner.nextLine();
        String task = scanner.nextLine();
        showUsers();
        logger.trace("Input user:");
        logger.trace("-->");
        int user = scanner.nextInt();
        logger.trace("Input deadline:");
        Integer deadline = scanner.nextInt();
        controller.addTo(task, user, deadline);
    }

    private void toDo() {
        controller.toDoState();
    }

    private void inProgress() {
        controller.inProgressState();
    }

    private void review() {
        controller.reviewState();
    }

    private void done() {
        controller.doneState();
    }

    private void interact() {
        int key;
        do {
            showMenu();
            logger.info("Input option: ");
            key = scanner.nextInt();
            methods.get(String.valueOf(key)).print();

        } while (key != 6);
    }
}
