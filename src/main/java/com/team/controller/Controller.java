package com.team.controller;

import com.team.state.State;
import com.team.state.ToDoState;
import com.team.task.Task;

import java.util.LinkedList;
import java.util.List;

public class Controller {

    private State state;
    private List<Task> tasks = new LinkedList<>();

    public Controller() {
        this.state = new ToDoState();
    }

    public final void setState(final State state) {
        this.state = state;
    }

    public final void addTask(final String name, final int userID, final Integer deadline) {
        tasks.add(new Task(name, userID, deadline));
    }

    public final void addTo(final String name, final int userID, final Integer deadline) {
        state.addTask(this, name, userID, deadline);
    }

    public final void toDoState() {
        state.goToDo(this);
    }

    public final void inProgressState() {
        state.goToInProgress(this);
    }

    public final void reviewState() {
        state.goToReview(this);
    }

    public final void doneState() {
        state.goToDone(this);
    }

    public final void setTasks(final List<Task> tasks) {
        this.tasks = tasks;
    }

    public final List<Task> getTasks() {
        return tasks;
    }
}
