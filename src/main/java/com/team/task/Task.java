package com.team.task;

import com.team.enums.User;

public class Task {

    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_RESET = "\u001B[0m";

    private String name;
    private int userID;
    private Integer deadline;
    private boolean done = false;

    public Task(final String name, final int userID, final Integer deadline) {
        this.name = name;
        this.userID = userID;
        this.deadline = deadline;
        Thread thread = new Thread(() -> {
            int time = deadline;
            while (time > 0 && !this.isDone()) {
                if (time == 1) {
                    System.out.println(ANSI_RED + "Only 1 day left to execute "
                            + name + " task.");
                    System.out.println(ANSI_RESET);
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                time--;
            }
        });
        thread.start();
    }

    public final boolean isDone() {
        return done;
    }

    public final void changeDone() {
        this.done = true;
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final User getUser() {
        return User.values()[userID - 1];
    }

    public final void setUser(final int userID) {
        this.userID = userID;
    }

    public final Integer getDeadline() {
        return deadline;
    }

    public final void setDeadline(final Integer deadline) {
        this.deadline = deadline;
    }

    @Override
    public final String toString() {
        return "Task{"
                + "name='" + name + '\''
                + ", user=" + User.values()[userID - 1]
                + ", deadline=" + deadline + '}';
    }
}
